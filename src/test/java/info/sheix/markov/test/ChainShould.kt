package info.sheix.markov.test

import info.sheix.markov.CharacterChain
import info.sheix.markov.GenericChain
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException
import java.util.stream.Collectors

class ChainShould {
    private lateinit var codeCounts: List<Char>

    @Before
    fun init() {
        codeCounts = listOf<Char>('c', 'o', 'd', 'e', ' ', 'c', 'o', 'u', 'n', 't', 's')
    }

    @Test
    fun readLine() {
        val chain = CharacterChain()
        chain.readGenericLine(codeCounts)
    }

    @Test
    fun afterReadLineReturnDEOnce() {
        val chain = CharacterChain()
        chain.readGenericLine(codeCounts)
        chain.getCount("de")
    }

    @Test
    fun createLine500charsLongForWalk500() {
        val chain = CharacterChain()
        chain.readGenericLine(codeCounts)
        val result = chain.walk(500)
        Assert.assertEquals(500, result.size.toLong())
    }

    @Test
    fun createLine500charsLongForWalk500Generic() {
        val chain = GenericChain('_')
        val codeCounts = "code counts"
        chain.readGenericLine(codeCounts.chars().mapToObj { c: Int -> c.toChar() }.collect(Collectors.toList()))
        val result = chain.walk(500)
        Assert.assertEquals(500, result.size.toLong())
    }

    @Test
    fun readFile() {
        val chain = CharacterChain()
        try {
            chain.readCorpus(File("planets.txt"))
        } catch (e: FileNotFoundException) {
            Assert.fail()
        }
        val result = chain.walk(500)
        Assert.assertEquals(500, result.size.toLong())
    }

    @Test
    fun next() {
        val chain = CharacterChain()
        try {
            chain.readCorpus(File("planets.txt"))
        } catch (e: FileNotFoundException) {
            Assert.fail()
        }
        for (i in 0..99) {
            println(chain.next)
        }
    }
}