package info.sheix.markov

import java.security.InvalidParameterException
import java.util.*

class WeightedNode<T : Any>() {
    private val r: Random = Random()
    private val nodes: MutableMap<T, WeightedNode<T>> = HashMap()
    private var weight = 0
    private var character: T? = null

    constructor(charAt: T) : this() {
        weight = 1
        character = charAt
    }

    private fun increaseWeight() {
        weight++
    }

    private fun getCount(c: T): Int {
        val n = nodes[c] ?: return 0
        return n.weight
    }

    fun add(first: T, second: T) {
        val n = nodes[first]
        if (n == null) nodes[first] = WeightedNode(first)
        val ngram = nodes[first]!!
        ngram.add(second)
    }

    private fun add(tail: T) {
        val node = nodes[tail]
        if (node == null) nodes[tail] = WeightedNode(tail) else node.increaseWeight()
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("==start==")
        sb.append(character)
        sb.append("==")
        for (c in nodes.keys) {
            sb.append("\n")
            sb.append(c).append(" : ").append(nodes[c].toString()).append("  ").append(weight)
        }
        sb.append("==-end-==")
        return sb.toString()
    }

    fun getCount(string: List<T>): Int {
        return if (string.size == 1) getCount(string[0]) else nodes[string[0]]!!.getCount(string.subList(1, string.size))
    }

    fun randomBranch(): T {
        val s = r.nextInt(nodes.values.stream().mapToInt { i: WeightedNode<T> -> i.weight }.sum())
        var i = 0
        for (item in nodes.keys) {
            val start = i
            while (i - start < nodes[item]!!.weight) {
                if (i == s) return item
                i++
            }
        }
        throw InvalidParameterException()
    }

    fun traverse(c: T): Optional<T> {
        val ngram = nodes[c] ?: return Optional.empty()
        return Optional.of(ngram.randomBranch())
    }
}