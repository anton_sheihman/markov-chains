package info.sheix.markov

import java.util.*

open class GenericChain<T : Any>(private val separator: T) {
    var root = WeightedNode<T>()

    fun readGenericLine(items: List<T>) {
        for (i in 0 until items.size - 1) {
            root.add(items[i], items[i + 1])
        }
        root.add(items[items.size - 1], separator)
    }

    fun walk(steps: Int): List<T> {
        var item = root.randomBranch()
        val result: MutableList<T> = ArrayList()
        for (i in 0 until steps) {
            result.add(item)
            val next: Optional<T> = if (item == separator) Optional.of(root.randomBranch()) else root.traverse(item)
            item = next.orElse(separator)
        }
        return result
    }

    fun walk(): List<T> {
        var item = root.randomBranch()
        val result: MutableList<T> = ArrayList()
        while (item != separator) {
            result.add(item)
            item = root.traverse(item).orElse(separator)
        }
        return result
    }
}