package info.sheix.markov

import java.io.File
import java.io.FileNotFoundException
import java.util.*
import java.util.stream.Collectors

class CharacterChain : GenericChain<Char>('_') {
    fun readCorpus(file: File) {
        val s = Scanner(file)
        while (s.hasNextLine()) {
            val line = s.next()
            if (line.isEmpty()) continue
            readGenericLine(line)
        }
    }

    fun readCorpus(corpus: String) {
        val split = corpus.split("\\r?\\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (line in split) {
            if (line.isEmpty()) continue
            readGenericLine(line)
        }
    }

    private fun readGenericLine(s: String) {
        readGenericLine(s.chars().mapToObj { c: Int -> c.toChar() }.collect(Collectors.toList()))
    }

    fun getCount(string: String) {
        val cList: MutableList<Char> = ArrayList()
        for (element in string) {
            cList.add(element)
        }
        root.getCount(cList)
    }

    val next: String
        get() {
            val sb = StringBuilder()
            for (c in walk()) {
                sb.append(c)
            }
            return sb.toString()
        }
}