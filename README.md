So, Markov Chains. Probabilistic model, used to make some statistics magics. Its like neural network, but random and simple. The more data you feed it, better it behaves. In game development used mostly by rogue-like writers for generation of funny location names.

The model eats sequences. They can be word sequences, numbers, images, other stuff sequences. Chain learns what usually comes after all, and, when asked to generate, just gets random piece from a sequence and puts it next in line. 

If you split "War and Peace" word by word and give it as an input to Markov Chain, you could get similar looking almost grammatically correct sentences in Russian and French. If you take all the planets and satellites names in Solar System and split it by letters, you'll possibly get some Ancient-Greek-Like names.

The model counts repetitions of sequences (like "h" comes after "t" in 50% of cases, and after "p" in 30%), and when letter "h" is given as a parameter, chain rolls a virtual dice and gives a next letter according to probability to meet it in learning corpus.

In my spare time I worked on library that should provide generic interface for such chains. 

Interface is pretty simple - 3 Steps - create chain, read training data and in the end let it run. 

Creation involves setting the separators for ordered lists. Use the following constructor:


```
#!java

    GenericChain<T> chain = new GenericChain(separator);

```

For example, in additional "CharacterChain" example class, these separators are set to "_", underscore characters.

Now model needs training. Use the following method:


```
#!java

    chain.readLine(line);

```

Adds a line as an example to the chain. Weights are increased for all transfers.
And the last method, which actually runs the chain:

    
```
#!java

List<T> result = chain.walk();

```

Will generate a sequence of randomly chosen node objects.

For now chain supports only first order generation.